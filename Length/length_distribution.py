# This code can not get the length perfectly for all time steps#
from __future__ import division
import os
import pandas as pd
import math
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 14}
matplotlib.rc('font', **font)


#------------Load data for a single simulations Flyerplate simulation---------------------------------#
#dir_name= '/Users/pranaychakraborty/myproject/Flyerplate/stress/alldamage/damageall/'
#sim_name = 'damage_3.csv'
#filename = str(dir_name + '/' + sim_name)
#df=pd.read_csv(filename, sep='\s+',header=None)
#column_names = ['x1','x2','y1','y2','dmg','time_step']
#df.columns = column_names
#df = df[['time_step','x1','y1','x2','y2','dmg']]
#df = df[(df.dmg > 1.9)]
#print('all points')
#print(len(df))

#----------------------------Data for 20 crack simulations-------------------#
dir_name='/Users/pranaychakraborty/myproject/DR_competition_paper-master/Data'
sim_name='2_3_20Crack_3_results.csv'
df = pd.read_csv(str(dir_name + '/' + sim_name))[['time_step','x1','y1','x2','y2','dmg']]
df = df[(df.dmg > 1.0)]

df['point1']=df['x1'].astype(str)+','+df['y1'].astype(str) #  take x1 and y1 to make point1
df['point2']=df['x2'].astype(str)+','+df['y2'].astype(str) # take x2 and y2 to make point2
time = []

fig = plt.figure(facecolor='white')


df = df[(df.time_step==200)] # Check a specific time frame e.g., 200
print('Intital points at a timestep')
print(len(df))

changeddf=df # This chnageddf will change as we already took the connected element i.e., connected element will be removed iteratively
count=0 # This is an index for connected fractured element, all connected element will have the same index
#print(changeddf)

data_you_need=pd.DataFrame()
#for i in range(0,len(df.x1)):
print('Before reomving unique points=')
print(len(changeddf))

#--------------------------Newly added part-------------------------------#

point1_check1=changeddf.point1.tolist()
point2_check2=changeddf.point2.tolist()
changeddf['check1'] = changeddf.point1.isin(point2_check2)
changeddf['check2'] = changeddf.point2.isin(point1_check1)
changeddf ['freq_point1'] = changeddf.groupby('point1')['point1'].transform('count')
changeddf ['freq_point2'] = changeddf.groupby('point2')['point2'].transform('count')
changeddf= changeddf[((changeddf["check1"] == True) | (changeddf["check2"] == True) | (changeddf["freq_point1"] > 2) | (changeddf["freq_point2"] > 2))]
#changeddf = changeddf
changeddf = changeddf[['time_step', 'x1', 'y1', 'x2', 'y2', 'dmg', 'point1', 'point2']].copy()
print('After removing unique points')
print(len(changeddf))
#--------------------------Newly added part finish------------------------#

while len(changeddf.x1) > 0:
    #count = count+i
    count = count + 1
    appended_data = []

    #currentdf=df.iloc[[i]]
    currentdf = changeddf.iloc[[0]]
    print('currentdf')
    print(currentdf)
    point1list=currentdf.point1.tolist()
    point2list=currentdf.point2.tolist()

    while len(changeddf[changeddf['point1'].isin(point1list)]) + len(changeddf[changeddf['point1'].isin(point2list)]) +\
    len(changeddf[changeddf['point2'].isin(point1list)])+len(changeddf[changeddf['point2'].isin(point2list)]) > 0:
        changeddf['check3'] = changeddf.point1.isin(point1list) # check if point1 repeats in point1 column
        changeddf['check4'] = changeddf.point1.isin(point2list) # check id point1 repeats in point2 column
        changeddf['check5'] = changeddf.point2.isin(point1list) # check if point2 repeats in point1 column
        changeddf['check6'] = changeddf.point2.isin(point2list) # check if point2 repeats in point2 column

        #print(changeddf)
        connectedlines1 = changeddf[((changeddf.check3==True) | (changeddf.check4==True) | (changeddf.check5==True) | (changeddf.check6==True))]
        connectedlines1['index1'] = count
        data_you_need=data_you_need.append(connectedlines1) #ignore_index=True was there
        point1list = connectedlines1.point1.tolist()
        point2list = connectedlines1.point2.tolist()
        changeddf = changeddf[((changeddf.check3==False) & (changeddf.check4==False) & (changeddf.check5==False) & (changeddf.check6==False))]
        # Just keep the segments that are not connected yet
    print('length after removing connected points')
    print(len(changeddf))
    #connectedlines[i]= data_you_need
    #count=i
print('Final chnageddf')
print(len(changeddf))

#data_you_need=data_you_need[(data_you_need.index > 2)] # I have to get rid of the borders find some good way to do that
data_you_need['length'] = data_you_need.apply(lambda row: math.sqrt((row.x1-row.x2)**2+(row.y1-row.y2)**2), axis=1)
#-----------------------------------------------------------------------------------------------#
data_you_need.to_csv('out.csv')
fig = plt.figure(0)
axis = fig.add_subplot(111)
axis.plot
axis.plot([data_you_need.x1, data_you_need.x2],[data_you_need.y1,data_you_need.y2])

plt.savefig('fracture_distribution.png')
plt.close(fig)
#-----------------------------------------------------------------------------------------------#

rows = data_you_need['index1'].nunique()
#print('What data I need')
#print(rows)
s = np.zeros((rows,2))
xx= np.zeros(rows)
yy=np.zeros(rows)
count=0
#for i in data_you_need.index1.unique():
newlist = []


#for x in range(3):
#    for y in range(3):
#        p='%f %f'%(x,y)
#        newlist.append(p)
#print(newlist)
rowname = data_you_need.index1.unique()
rownumbers = len(rowname)
print('total connected fractures')
print(rownumbers)



count=0
for i in range(0,rownumbers):
    cdf=data_you_need[(data_you_need.index1 ==rowname[i])]
    totallength= cdf['length'].sum()/2
    if totallength <= 80:
        xx[i]=count
        yy[i]=totallength
        count=count+1
        #print(count)

fig = plt.figure(0)
axis = fig.add_subplot(111)
axis.plot
axis.plot(xx,yy,marker='o',linestyle = 'None')
plt.ylim(0, 2)
plt.savefig('fracture_length.png')
print('Working!')
'''

for i in range(0,rows):
    cdf=data_you_need[(data_you_need.index1 ==i)]
    totallength= cdf['length'].sum()
    if totallength < 10 and totallength >0:
        p='%f %f'%(count,totallength)
        #print(p)
        newlist.append(p)
    count=count+1
print(newlist)
print('How many fractures')
print(len(newlist))
print('working')
'''

# This code has many bugs which are not fixed yet#


from __future__ import division
import os
import pandas as pd
import math
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#plt.style.use('ggplot')
#from matplotlib.ticker import ScalarFormatter,AutoMinorLocator
#dirFile = os.path.dirname(os.path.join('/Users/pranaychakraborty/myproject/Flyerplate/nucleation_flyerplate/','NicePlotProductivity.py'))
#plt.style.use(os.path.join(dirFile))
font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 14}
matplotlib.rc('font', **font)

fig = plt.figure(facecolor='white')
plt.tight_layout()
#------------Load data for a single simulations Flyerplate simulation---------------------------------#
dir_name= '/Users/pranaychakraborty/myproject/Flyerplate/stress/alldamage/damageall/'
sim_name = 'damage_3.csv'
filename = str(dir_name + '/' + sim_name)
dfin=pd.read_csv(filename, sep='\s+',header=None)
column_names = ['x1','x2','y1','y2','dmg','time_step']
dfin.columns = column_names
dfin = dfin[['time_step','x1','y1','x2','y2','dmg']]
dyrange  = np.arange(0, 20, 2.5)

for j in range(0,len(dyrange)-1):
    ytop = dyrange[j+1]
    ybot = dyrange[j]

    df = dfin[((dfin.dmg > 1.5) & (dfin.y1 > ybot) & (dfin.y2 > ybot) & (dfin.y1 < ytop) & (dfin.y2 < ytop))]
    #----------------------------Data for 20 crack simulations-------------------#
    #dir_name='/Users/pranaychakraborty/myproject/DR_competition_paper-master/Data'
    #sim_name='2_3_20Crack_47_results.csv'
    #df = pd.read_csv(str(dir_name + '/' + sim_name))[['time_step','x1','y1','x2','y2','dmg']]
    #df = df[(df.dmg > 0)]

    df['point1']=df['x1'].astype(str)+'_'+df['y1'].astype(str)
    df['point2']=df['x2'].astype(str)+'_'+df['y2'].astype(str)
    time = []
    elementcount = []

    #df['freq_point1'] = df.groupby('point1')['point1'].transform('count')
    #df['freq_point2'] = df.groupby('point2')['point2'].transform('count')

    for i in range(0,201):
        HOSS_data = df[(df.time_step == i)]
        #print('Before frequency count')
        #print(len(HOSS_data.x1))
        HOSS_data ['freq_point1'] = HOSS_data.groupby('point1')['point1'].transform('count')
        HOSS_data ['freq_point2'] = HOSS_data.groupby('point2')['point2'].transform('count')
        #print('Before frequency count')
        #print(len(HOSS_data.x1))
        total_damged_element= len(HOSS_data.dmg)
        #print('total_damged_element=%d'%(total_damged_element))
    #check = HOSS_data.point1.isin(HOSS_data.point2)
        point1_check1=HOSS_data.point1.tolist()
        point2_check2=HOSS_data.point2.tolist()
        HOSS_data['check1'] = HOSS_data.point1.isin(point2_check2)
        HOSS_data['check2'] = HOSS_data.point2.isin(point1_check1)
        #HOSS_data['check1'] = HOSS_data.point1.isin(HOSS_data.point2)
        #HOSS_data['check2'] = HOSS_data.point2.isin(HOSS_data.point1)

        #uniquepoint1=HOSS_data.point1.unique()
        #HOSS_data['check3'] = HOSS_data.point1.isin(uniquepoint1)
    #print(HOSS_data)
        HOSS_data_nucleated= HOSS_data[((HOSS_data["check1"] == False) & (HOSS_data["check2"] == False) & (HOSS_data["freq_point1"] <= 2) & (HOSS_data["freq_point2"] <= 2))]
        count_nucleated = len(HOSS_data_nucleated.dmg)
        #print('total nucleated fractures =%d'%(count_nucleated))
        fraction_nucleated = (count_nucleated / total_damged_element)*100
        #print('fraction nucleated = %f'%(fraction_nucleated))
        time.append(i)
        elementcount.append(fraction_nucleated)
        #print(HOSS_data)
    #print(time)
    #print(elementcount)
    plt.plot(time,elementcount,label='%s - %s' % (ybot,ytop))
plt.legend()
    #plt.legend(['%d zone' %j])
plt.xlabel("Time")
plt.ylabel("% nucleated element")
plt.title(" Simulation no 3 (dmg > 1.5)")
#plt.xlabel('Time (S)')
#plt.ylabel('N$_$nu/N')
plt.savefig('nucleation_flyer_zone_all.png',dpi=700,facecolor='white')

#----------------------------------------------------Best way------------------------------------------------#
# Read the code in the "vietscode folder" and write your own code to get all the data for all the simulations.
#------------------------------------------------------------------------------------------------------------#



#-----------------------------------------------------------------------------------------------------------#
To  get all the data for all the simulations you may use the following instructions and example or write your own code from scratch.
If you write your own code, still you can use the job submission script (from example folder) as they have all the required modules added.
#-----------------------------------------------------------------------------------------------------------#


collectdamage: See the folder
#--------------Editing in the templatedamage1.txt file------------------#
# Change input file directory in "filename" variable. filename will have the location of your pvtu files. (In line no 130 and 134)
# Change the location where you want to save your data. "directorypath " will have the location of your output data (In line no 140)
#---------------Editing the job.sh file---------------------------------#
# In line no 17 replace "201" with number of time steps in your own simulations.

# --------------------Edit the createdamage1.py-------------------------#
Change N. N is the number of simulations


#---------------------------------Running the codes----------------------#
Step 1: python createdamage1.py (This command will create N python(.py) files, N=Number of simulations)
Step 2: python createjob.py (This will create job submission script (.sh) for for all python files created in step 1)
Step 3: submit all the jobs (.sh) and the damage data will be saved in one folder for each simulation. It will create files for each time step.
We have to compile them in one file for each simulation.
See the example1 folder to see how the files look like
#-------------------------------------------------------------------------#



compiledamage: See the folder
#----------------------------Compiling data for one simulation to one file (It will have all the timesteps)-------------------------#
# Please see the "compiledamage" folder for example templates.
# Step 1: python create.py (This command will create many python files. Each python file will compile data for a single simulation to one file)
# Step 2: python createjob.py (It will create all the job scripts based on the template job.sh).

import sys

import os

import time

import numpy as np





tic = time.time()



# Nvtu is the total number of .pvtu files

Nvtu = int(sys.argv[1])





# import paraview packages

from paraview.simple import *

from paraview.numpy_support import vtk_to_numpy 



# mpi4py

from mpi4py import MPI

comm = MPI.COMM_WORLD

rank = comm.Get_rank()

size = comm.Get_size()



# filter data by celltag

def FILTERVTK(filename):

    pvtu = XMLPartitionedUnstructuredGridReader(FileName=filename)

    pvtu_fetch = servermanager.Fetch(pvtu)

    return pvtu_fetch



# get two ends coordinate of cell (bar type)

def cellcoord(nodalcoord):

    nodalcoord = nodalcoord[:,:2]

    nodalcoord = nodalcoord.reshape((int(nodalcoord.shape[0]/2), 4))

    x1 =  nodalcoord[:,0] 

    x2 =  nodalcoord[:,1] 

    y1 =  nodalcoord[:,2] 

    y2 =  nodalcoord[:,3]

    return [x1,y1,x2,y2]







# calculate strain and kinetic energy

def GETDAMAGE(filename):

    pvtu_fetch = FILTERVTK(filename)

    # cell data

    pvtu_cell = pvtu_fetch.GetCellData()

    damage = pvtu_cell.GetArray('Damage')

    damage = vtk_to_numpy(damage)



    # points data

    nodalcoord = pvtu_fetch.GetPoints().GetData()

    nodalcoord = vtk_to_numpy(nodalcoord)

    

    # area of triangles

    x1,y1,x2,y2 = cellcoord(nodalcoord)

    return [x1,y1,x2,y2,damage]






#os.mkdir('/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Pranay/results/simulation_99/')


k=1 # Simulation number
# split job

for j in range(Nvtu):

	if j % size == rank:

		ivtu = j

		if ivtu < 10000:

			filename = '/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Flyer_Plate_Exp_Cracks_{:d}/Sfrc_{:04d}.pvtu'.format(k,ivtu)

		else:

			filename = '/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Flyer_Plate_Exp_Cracks_{:d}/Sfrc_{:05d}.pvtu'.format(k,ivtu)

		[x1,y1,x2,y2,damage] = GETDAMAGE(filename) 

		

		directorypath = '/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Pranay/100sims/results/simulation_{:d}'.format(k)
                if not os.path.exists(directorypath):
                    os.makedirs(directorypath)   
		stack_all = np.stack((x1,y1,x2,y2,damage), axis=1)
                outputtxt = '/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Pranay/100sims/results/simulation_{:d}/Sfrc_{:04d}_{:04d}.csv'.format(k,k,j)
                stack_all1= np.append(stack_all,np.zeros([len(stack_all),1]),1)
                stack_all1[:,5]=j
                #if j==0:
                #    stack_all2= np.append(stack_all,np.zeros([len(stack_all),1]),1)
                #    stack_all2= np.append(stack_all,np.zeros([len(stack_all),1]),1)
                #else:
                #    stack_all2= np.r_[stack_all2, stack_all1]
                #    print(len(stack_all2))
                #print(numrows)
                #for k in range(0)
		np.savetxt(outputtxt, stack_all1, fmt=['%14.6E', '%14.6E', '%14.6E', '%14.6E', '%14.6E','%d'])
#outputtxt2 = 'Sfrc_check.csv'
#np.savetxt(outputtxt2, stack_all2, fmt=['%14.6E', '%14.6E', '%14.6E', '%14.6E', '%14.6E','%d'])



if rank == 0:   

    toc = time.time()

    print('----- {:.2f} sec -----'.format(toc-tic))



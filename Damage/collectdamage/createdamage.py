import os
N = 100 # How many files you want to create and it should equal to number of simulations
onames = [];
for i in range(1, N):
    directory ='/net/scratch3/erougier/LDRD_DR_DYNGR/100SIMS_FlyerPlate/Flyer_Plate_Exp_Cracks_{:d}/hossdata{:d}.py'.format(i,i)
    inputfile = open("templatedamage.txt", "r")
    outputfile = open("mc"+str(i)+".py","w")
    outputfile = open(directory,"w")
    for line in inputfile:
       line = line.replace('##MYOUTDIR##', 'N00'+str(i))
       outputfile.write(line)

import os
N = 2 # It is equal to the number of simulations
onames = [];
for i in range(1, N):
    directory ='hossdata{:d}.py'.format(i) # Output file name based on simulation number
    inputfile = open("templatedamage1.txt", "r") # It reads the template file
    outputfile = open(directory,"w")
    for line in inputfile:
       line = line.replace('xxxx',str(i))
       outputfile.write(line)

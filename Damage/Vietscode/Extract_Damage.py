import sys
import os
import time
import numpy as np


tic = time.time()

# Nvtu is the total number of .pvtu files
Nvtu = int(sys.argv[1])


# import paraview packages
from paraview.simple import *
from paraview.numpy_support import vtk_to_numpy 

# mpi4py
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# filter data by celltag
def FILTERVTK(filename):
    pvtu = XMLPartitionedUnstructuredGridReader(FileName=filename)
    pvtu_fetch = servermanager.Fetch(pvtu)
    return pvtu_fetch

# get two ends coordinate of cell (bar type)
def cellcoord(nodalcoord):
    nodalcoord = nodalcoord[:,:2]
    nodalcoord = nodalcoord.reshape((int(nodalcoord.shape[0]/2), 4))
    x1 =  nodalcoord[:,0] 
    x2 =  nodalcoord[:,1] 
    y1 =  nodalcoord[:,2] 
    y2 =  nodalcoord[:,3]
    return [x1,y1,x2,y2]



# calculate strain and kinetic energy
def GETDAMAGE(filename):
    pvtu_fetch = FILTERVTK(filename)
    # cell data
    pvtu_cell = pvtu_fetch.GetCellData()
    damage = pvtu_cell.GetArray('Damage')
    damage = vtk_to_numpy(damage)

    # points data
    nodalcoord = pvtu_fetch.GetPoints().GetData()
    nodalcoord = vtk_to_numpy(nodalcoord)
    
    # area of triangles
    x1,y1,x2,y2 = cellcoord(nodalcoord)
    return [x1,y1,x2,y2,damage]




# split job
for j in range(Nvtu):
	if j % size == rank:
		ivtu = j
		if ivtu < 10000:
			filename = 'Sfrc_{:04d}.pvtu'.format(ivtu)
		else:
			filename = 'Sfrc_{:05d}.pvtu'.format(ivtu)
		[x1,y1,x2,y2,damage] = GETDAMAGE(filename) 
		
		outputtxt = 'Sfrc_{:04d}.csv'.format(j)
		stack_all = np.stack((x1,y1,x2,y2,damage), axis=1)
		np.savetxt(outputtxt, stack_all, fmt=['%14.6E', '%14.6E', '%14.6E', '%14.6E', '%14.6E'])

if rank == 0:   
    toc = time.time()
    print('----- {:.2f} sec -----'.format(toc-tic))


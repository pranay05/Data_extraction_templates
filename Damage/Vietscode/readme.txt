This is the readme file showing how to extract damage value of each element line segment. There will be duplicate values since there area shared zone between to elements.

To find job submission script see example folder.
 
This script can be easily modified to get other data from the .vtu files.
Here are the steps to follow:

1. Copy the Extract_Damage.py to the folder containing all .vtu files.

2. Allocate 1 node for running in parallel:
   salloc -N 1 -t 1:00:00 --qos=interactive

3. Load the following modules
   module load openmpi/1.10.5
   module load python/2.7-anaconda-4.1.1
   module load paraview/5.4.0-osmesa

4. Run the code using the mpi command
   mpirun -n ##1 python Extract_Damage.py ##2
   ##1: number of processors
   ##2: number of .pvtu files

If you need any assistance, please contact Viet Chau at vchau@lanl.gov or ahunter@lanl.gov
   

  

import sys
import os
import time
import numpy as np


tic = time.time()

# Nvtu is the total number of .pvtu files
Nvtu = int(sys.argv[1])


# import paraview packages
from paraview.simple import *
from paraview.numpy_support import vtk_to_numpy 

# mpi4py
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# filter data by celltag
def FILTERVTK(filename, celltag1, celltag2):
    pvtu = XMLPartitionedUnstructuredGridReader(FileName=filename)
    # filter data using threshold in Paraview
    pvtu_threshold = Threshold(Input=pvtu)
    pvtu_threshold.Scalars = ['CELLS', 'CellTag']
    pvtu_threshold.ThresholdRange = [celltag1, celltag2]
    pvtu_fetch = servermanager.Fetch(pvtu_threshold)
    return pvtu_fetch

# calculate centroid of triangles
def CENTROID(nodalcoord):
    nodalcoord = nodalcoord[:,:2]
    nodalcoord = nodalcoord.reshape((int(nodalcoord.shape[0]/3), 6))
    xc = (nodalcoord[:,0] + nodalcoord[:,2] + nodalcoord[:,4])/3.0    
    yc = (nodalcoord[:,1] + nodalcoord[:,3] + nodalcoord[:,5])/3.0
    return [xc,yc]



# calculate strain and kinetic energy
def GETSTRESS(filename, celltag1, celltag2):
    pvtu_fetch = FILTERVTK(filename, celltag1, celltag2)
    # cell data
    pvtu_cell = pvtu_fetch.GetCellData()
    stress1 = pvtu_cell.GetArray('CauchyStress_1')
    stress1 = vtk_to_numpy(stress1)
    stress12 = pvtu_cell.GetArray('CauchyStress_12')
    stress12 = vtk_to_numpy(stress12)
    stress2 = pvtu_cell.GetArray('CauchyStress_2')
    stress2 = vtk_to_numpy(stress2)
    # points data
    nodalcoord = pvtu_fetch.GetPoints().GetData()
    nodalcoord = vtk_to_numpy(nodalcoord)
    
    # area of triangles
    xc,yc = CENTROID(nodalcoord)
    return [xc, yc, stress1, stress2, stress12]


# cell tag for vtk file filter
celltag1 = 1.0  
celltag2 = 1.0



# split job
for j in range(Nvtu):
	if j % size == rank:
		ivtu = j
		if ivtu < 10000:
			filename = 'Sout_{:04d}.pvtu'.format(ivtu)
		else:
			filename = 'Sout_{:05d}.pvtu'.format(ivtu)
		xc, yc, stress1, stress2, stress12 = GETSTRESS(filename, celltag1, celltag2) 
		
		outputtxt = 'Sout_{:04d}.csv'.format(j)
		stack_all = np.stack((xc, yc, stress1, stress2, stress12), axis=1)
		np.savetxt(outputtxt, stack_all, fmt=['%14.6E', '%14.6E', '%14.6E', '%14.6E', '%14.6E'])

if rank == 0:   
    toc = time.time()
    print('----- {:.2f} sec -----'.format(toc-tic))

              
